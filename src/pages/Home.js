import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Course from '../components/Course'

export default function Home() {
	return(
		<Fragment>
		<Banner/>
		<Highlights/>
		<Course/>
		</Fragment>
		)
}